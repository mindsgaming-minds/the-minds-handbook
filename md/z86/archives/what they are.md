# Minds Tokens: What They Are and What They Are Not

------

About a month ago now, Minds [implemented a ‘crypto’ framework](https://www.minds.com/blog/view/793221494705606656) in a very large software upgrade. Since then, there have been numerous questions and references as to what Minds Tokens are, and what they are worth. Minds has promised another guide with many answers we have had, but we have no idea about when said guide might appear; we all remember the ‘white paper’ waiting game right? So, in this post I’ll attempt to answer some of these questions with facts rather than speculation.

Firstly, it’s important to understand Minds is currently on the [Rinkeby Testnet.](https://www.rinkeby.io/#stats) While we are on this testnet, Minds developers are not only testing the system for bugs, but measuring various metrics and making critical adjustments. The important thing to understand is all the Minds Tokens we are using while on the testnet, are play tokens. We are beta-testing the new Minds Crypto framework. Snapshots were taken at a couple of different intervals prior to the Crypto release, and our Wallets will be restored to those snapshots when we go live, as such [we have been told](https://www.minds.com/blog/view/804849279664717824).

![](https://www.minds.com/fs/v1/thumbnail/836601180562989056)

## What are Minds Tokens:

[Minds Tokens](https://rinkeby.etherscan.io/token/0xf5f7ad7d2c37cae59207af43d0beb4b361fb9ec8) are ERC-20 compliant tokens, meaning they follow the
[ERC-20 standard](https://theethereum.wiki/w/index.php/ERC20_Token_Standard). While Minds Tokens reside on the [Ethereum](https://en.wikipedia.org/wiki/Ether) blockchain, they are not to be confused as Ether for which they are certainly not. An important difference between Minds Tokens and Ether is where they come from, and how they earn their value.

Ether is an actual [cryptocurrency](https://en.wikipedia.org/wiki/Cryptocurrency), and like all cryptocurrencies are mined. It is the mining process that determines the value of a cryptocurrency. Minds Tokens are not mined, but rather created. Minds can create as many tokens as they want, and they also set the value of their tokens.

![](https://www.minds.com/fs/v1/thumbnail/836603487572795392)

## What value do Minds Tokens have?

Even though we are on a test net, and our tokens don’t have a monetary value, they do in fact have one value: One Minds Token has the value to [exchange for 1000 views](https://www.minds.com/wallet/tokens/contributions). In fact it’s currently the only way to purchase a [Boost](https://www.minds.com/boost/console/newsfeed/history;toggled=true). 
So when we employ the [WIRE](https://www.minds.com/wire) feature, we are in effect sending each other the ability to Boost. The way I see it, is thats a pretty big value in itself as Boosting is a HUGE part of what we do on Minds. Other than that, Minds Tokens don't have any other intrinsic value that I can ascertain. To be clear, at this point and time, Minds Tokens do NOT have a monetary value; ie. you can’t exchange your Minds Tokens for money or Ether.

## What value will Minds Tokens have in the future?

This is something we don't know. There will of course be some relationship between Minds Tokens, cash, Ether, and various rewards. That said, there is no possible way to know any of what 
these relationships might be, as we don't know what Minds will set the value of their tokens at, nor the criteria used in setting that value. I will hazard that will be released in the guide I
mentioned in my opening paragraph.

What we need to know is it's important for us to use our 'play tokens' on the Rinkeby Testnet, so we can help Minds find and fix any bugs that may be present, and to help them collect whatever
metrics they need so as to give our tokens value, beyond 1000 views, when we finally go live. We need to know that Minds ERC-20 compliant tokens are created, not mined like all cryptocurrencies
are, but created and given an arbitrary value not determined by the mining process. Also the only value MindsTokens have to date is 1000 views. Until we go live, Minds Tokens can not be converted
into Ether, and thus can not be converted into any sort of spendable money.

All we can do now, is wait for the promised 'guide' telling us what the full game really is.

---
Credits: [Luculent](https://www.minds.com/Luculent) on [Minds](https://www.minds.com/blog/view/826603426856132608) - Apr 27, 2018, 10:17:45 AM