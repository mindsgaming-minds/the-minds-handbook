### Collect support in ETH!
--------------------------


## Demonetization

Looking to monetize you channel, well not here on Minds. Minds offers ERC-20 tokens so other than selling these tokens on exchanges how can you make a little revenue on Minds? P2P with ETH!

### Tipping & Receiver Address

We recently made a post in the Minds “Help & Support” group asking why users can not use the minds WIRE system to send and receive ETH tips on P2P. Simply put Minds doesn’t want to register its currency or become a money transmission business (msb) per US FINCEN regulations.



## What about creators?

It seems Minds has forgot about it’s most valued users, that is its creators, the ones who add the great content you are looking for everyday. So how do you support these wonderful account skip the middle man and wire send your favorite creator a tip!


### Receiver address

As we can’t use the Minds WIRE system to send ETH to other users we are going to need a receiver address ask your favorite creator to list there ETH receiver address.


### Tips

Okay so we can’t send ETH p2p using the Minds Wire system, luckily we already have all the tools we need, our crypto wallet.

When you enrolled in Minds rewards you had to assign a receiver address this receiver address is where you store your Minds Tokens on-chain to trade, sell, exchange or whatever in a decentralized manner, well the good is this same wallet accepts ETH!

That’s right simply by provide your receiver address to your supporters! Note you are asking for ETH not tokens, we’d recommend a wallet that support both so you don’t miss out if someone makes a mistake.

Provided it in your bio, blogs, video descriptions and anywhere else you are looking for support.

Note you are looking for ETH support!


# Is it Safe?

It is safe to display your wallet receiver address for a user to access it they would need your account name and password, and passphrase. All addresses are public ledger so users will be able to see your events, you can get a masker or forwarder if your worried about this.

# Send ETH HERE:
0x2558d603DF1435d767408eBeca170ef640A9Ecb9