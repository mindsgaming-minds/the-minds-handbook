# The Guide

Original Links for information provided in this handbook:


### Master Blogs

1.) [Setting up your wallet for tokens](https://www.minds.com/blog/view/825462048806694912)

2.) [Learn the basics of Minds Token And ETH](https://www.minds.com/MindsGaming/blog/bank-account-what-eth-tokens-875109440196468736)

3.) [Learn about your paraphrase now!](https://www.minds.com/blog/view/842814141049421824)

4.) [Minds Is NOT Montized](https://www.minds.com/MindsGaming/blog/minds-is-not-montized-not-clickbait-1000844374174236672)

5.) [Can't Monetize! - What now?](https://www.minds.com/blog/view/877541807020875776)

6.) [Converting Minds Tokens To ETH, BTC, And More.](https://www.minds.com/MindsGaming/blog/converting-minds-tokens-to-eth-btc-and-more-piggy-backing-on-1038207636122226688)

7.) [Why Minds Requires Your Phone Number: Privacy and Security](https://www.minds.com/blog/view/826603426856132608)

8.) [Is Your Mind Now In Crypto Meltdown? Relax, Breath, Learn!](https://www.minds.com/blog/view/825693314038353920)

9.) [Minds Tokens: What They Are and What They Are Not](https://www.minds.com/blog/view/836607056186159104)


### More Helpful Links

1.) [Rewards](https://www.minds.com/wallet/tokens/contributions)

2.) [Wire Tokens](https://minds.com/wire)

2a.) [Offer post boosts](https://minds.com/boost)

3.) [Withdraw from your wallet](https://www.minds.com/wallet/tokens/withdraw)

4.) [Set up and use rewards under users bio](https://www.minds.com/blog/view/744654595174703104)

5.) [Create memes & user ads for the new token (Get the word out)](https://www.minds.com/groups/profile/568271817706319872/activity)

6.) [Help other users with the new token, don’t be afraid to ask questions yourself.](https://www.minds.com/groups/profile/773612857802629140)

7.) [Minds Token Current Standing](https://rinkeby.etherscan.io/token/0xf5f7ad7d2c37cae59207af43d0beb4b361fb9ec8)

8.) [WhitePaper](https://cdn-assets.minds.com/front/dist/assets/whitepapers/03_27_18_Minds%20Whitepaper%20V0.1.pdf)

9.) [Crypto launch, new apps and more!](https://www.minds.com/blog/view/826188573910073344)

10.) [Token 101](https://www.minds.com/wallet/tokens/101)

11.) [Minds Token Page](https://minds.com/token)

---

## Other Useful Links

---


- [Password Reset](https://www.minds.com/forgot-password) 

Reset your password.



- [Setting Up Rewards](/#user-docs-rewards)

This goes over how to support other channels



- [Your Minds Boost Console: How To Use It](/#user-docs-boost-console)

This covers how to use your boost console & track boosted posts.



[Blocking & Reporting](/#user-docs-blocking)

This covers how to block channels

---