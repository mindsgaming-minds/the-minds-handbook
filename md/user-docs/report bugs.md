# Reporting Bugs To Minds

Minds has a few ways to report issues with the service. 

---

1.) [Help & Support](https://www.minds.com/groups/profile/100000000000000681/feed)

You can post about the issue in the Minds "Help & Support" group and get a responce from an admin or in the support chat to ask some other Minds members about the issue.


2.) [Help Desk](https://www.minds.com/help)

For the more technical user you can [report bugs](https://www.minds.com/issues/report) to the Minds Gitlab through the help desk and search for answers on questions about the network.


3.) [Gitlab](https://gitlab.com/minds) 

You can also report issues directly from Gitlab if you have an account with Gitlab and want the more technical updates on your issue.

4.) Email

Lastly you can email Minds at **info@minds.com** or **security@minds.com** for issues or concerns about the network.

---