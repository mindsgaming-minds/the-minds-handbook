# Why Minds Requires Your Phone Number: Privacy and Security

----

Why do I need to provide my phone number to Minds in order to monetize my channel and to receive rewards?

In the past, prior to the Minds first quarter update of 2018, in order to monetize our channels, Minds used a third-party provider, a centralized payment system as it’s payment processing 
service.

Stripe required us to provide all the private information any bank would require of you when you opened an account. Needless to say, that included our Federal Identification numbers as well as 
other very sensitive data.

The collection of private data required by [Stripe](https://stripe.com/), which by the way is diametrically opposed to the Minds foundational paradigms of anonymity decentralization, and privacy, 
was not the only 
The collection of private data required by, which by the way is diametrically opposed to the Minds foundational paradigms of anonymity decentralization, and privacy, was not the only 
issue Minds users had with Stripe. Perhaps just as important, as a global community, outside of the [20 or so countries provided](https://stripe.com/global) for, Stripe did not have support for our entire community. 
This left millions of channels unable to monetize, crippling their ability to prosper from any sort of monetary earning by using Minds.

Further, the [ERC 20 token standard](https://theethereum.wiki/w/index.php/ERC20_Token_Standard) allows for autonomous peer to peer transactions (which is why Minds used Stripe as a consolation in the first place over providers such as PayPal). 
These are the reasons Minds ditched the centralized Stripe; they just did no fit into the Minds paradigm.

Those days are now over.

Today, after the first quarter update of 2018, when you log onto Minds, you will see that every Minds community member can now monetize their channels, no matter what country they happen to live in.
Perhaps more importantly, no longer are you required to provide all of your personal information when you set up monetization on your channel; all you need provide is the phone number of a device 
for which you can receive a text (or in many cases audible) code.

## Why do you need to provide a phone number?

Firstly, it’s very important to understand how your phone number is used, as well as why it’s needed in the first place.

When Minds receives your phone number, it's treated just as a password would be treated. You phone number is directly [hashed](https://simple.wikipedia.org/wiki/Cryptographic_hash_function) (and [salted](https://en.wikipedia.org/wiki/Salt_(cryptography)) ) on the fly, 
then stored in a secure database. No raw or clear-text numbers are stored anywhere, and Minds never sees your phone number; nor can it be used for any purpose other 
than as a unique identifier for you as an anonymous individual.

This is necessary for a couple of reasons. Understand, Minds is an anonymous social media, and because of that it needs to have a way of identifying you as an individual, 
for the proper handling of monetary transactions.

Under the old system, the system prior to the first quarter update of 2018, Minds was heavily challenged by users gaming, or scamming the system for points. Some users were creating 
multiple fake accounts, then relying heavily upon various exploits to obtain illegitimate gains. Many of these channels received warnings, and others were completely banned from Minds altogether.

That said, you can see under the crypto-currency model, this could amount to unlimited monetary damages, which would completely undermine the Minds crypto-currency completely.

You need to provide your phone number if you want to monetize your channel. This method uniquely identifies you, for the purpose of monetizing (as well as qualifying you to receive rewards), 
is a far less invasion of your privacy than it was prior to the update. Minds has chosen to use your phone number when you monetize, as a secure way to uniquely identify your account.

You will also need to provide your phone number if you wish to participate in any part of the Minds rewarding platform. This is due to the fact that unchained tokens can be converted into 
onchain tokens. Also it will all but eliminate the creation of fake accounts designed to game or exploit the system for illegitimate gains.

The Minds administrators have made these decisions to prevent exploitation of the system, and to stave off corruption of the Minds crypto-currency itself.

This is pretty good for us as a community. However, in the crypto world, nothing is set in stone. The handling of your phone number is a[] Know Your Customer]()https://en.wikipedia.org/wiki/Know_your_customer (KYC) process. KYC is the process of a business identifying and verifying the identity of its clients. The term is also used to refer to the bank and anti-money laundering regulations which governs these activities.

As you may have read, there is a LOT of discussion over what governmental agencies will come to enforce crypto-currency providers to collect for these purposes. Should law-makers and/or bureaucrats decide more customer information needs to be required, Minds will of course need to comply. Should something like this happen, I have been told these changes would only be at the withdrawal stage.

You should now understand more about how and why Minds is using your phone number, I hope you are quite a bit more at ease. Now go monetize your channels and reap some of the unique and innovative features Minds offers!

---
Credits: [Luculent](https://www.minds.com/Luculent) on [Minds](https://www.minds.com/blog/view/826603426856132608)