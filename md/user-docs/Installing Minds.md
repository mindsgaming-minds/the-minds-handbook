# GitLab Minds.com Install Guide
- You can find the Minds install guide on Minds [GitLab](https://gitlab.com/minds/minds)

## Changelog 
- Removed user install guide as it was no longer vaild (Working), please use Minds install from GitLab.

---
[Next! ->](/#user-docs-docs)