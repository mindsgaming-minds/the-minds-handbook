# Your Minds Boost Console: How To Use It

---



If you have not yet noticed, there will only be two 'notifications' for Boosts from now on: 1) When your Boost has been fulfilled (applies once all of your requested views have been met), and 
2) If your post happens to be rejected for boosting.

This means the method of keeping track of the status of both your Boosts, as well as the points associated with your Boosts will need to change. You will finally need to know how to use your 
Boost Console.
![](https://www.minds.com/fs/v1/thumbnail/758482163984244736/xlarge)

You get to your Boost Console by first clicking on the small 'Bank' icon, which represents your 'Wallet', in the top right-hand corner of your screen; near your avatar. Then in the left-hand 
side-bar you click on 'Boost' - Your Boost Console is now open.

In the top bar, you will see the three typs of Boosts you can make on Minds: NEWSFEEDS - These are posts that appear in our newsfeeds, or status posts. SIDEBARS - Blog posts and our 
Channel Boosts fall under this header. CHANNELS - These Boosts used to be referred to, or called, Peer-to-Peer boosts.

![](https://www.minds.com/fs/v1/thumbnail/758484962205769728/xlarge)

In the Image above, I have Boosted a status post, (notice the hilight on NEWSFEEDS in the upper bar). At the bottom of the post is all the information you'll want to know about your Boost, 
most importantly the status, the target number of views, and the actual number of views.

The next aspect you'll want to keep track of is the points you spent immediately after making your Boost. The Points Console is as easy to reach as your Boost Console. 
You get there from your Boost Console, then selecting 'Points Ledger' from the left side-bar.

![](https://www.minds.com/fs/v1/thumbnail/758487251498835968/xlarge)

Click on 'Points' in the upper blue bar. Once in your Points Console, you can see a perfect accounting of your points.

![](https://www.minds.com/fs/v1/thumbnail/758488070206005248/xlarge)

As you can see in the Image above, I have spent 2000 points on a post. Everything is time-stamped so you can easily corralate your Boosts and the points you spent on them.

The loss of many notifications with respect to Boosts is good for us as notifications are very resource intensive, and lend to bogging down the system. The Boost and Point Consoles consolidate 
all the information you need to keep track of your Boosts and the points you spent on your Boost.

If you have any questions on how to use either of these two management consoles, feel free to ask your questions in the comments, or on the official [Help & Support](https://www.minds.com/groups/profile/100000000000000681/activity) group page.

---
Credits: [Luculent](https://www.minds.com/Luculent) on [Minds](https://www.minds.com/blog/view/758490546346926080)