## Chapter-2 Creating an Account


To start creating your accountgo to [https://www.minds.com/login](https://www.minds.com/login)
After you have singed-up with Minds you will be given some channel links to edit & design your channel, we will go over them here as well as a few other hints and tricks.

You will first be given a chance a screen to select "what you are interested in". Select some topics to be pushed boosts from these areas first on your news-feed.  You can also select all or none to be pushed all boosts equally.
![](https://www.minds.com/fs/v1/thumbnail/795697338780024832)
-Select some topics.


After you have chosen your topics you will see profile links on your channel, the first being "design your channel" click this fake post to start editing your channel.

![](https://www.minds.com/fs/v1/thumbnail/795697949936754688)
-Design your channel.


Now that you are in edit mode you can do things like add a channel banner, just click the banner area and upload a image from your computer.
![](https://www.minds.com/fs/v1/thumbnail/795698901699330048)
-Add a banner.


The same process is applied when adding an icon, this image will show on all your posts and comments, just click the icon area and upload a image from your computer.
![](https://www.minds.com/fs/v1/thumbnail/795699270806970368)
-Add an icon.


After you have uploaded your icon go to the empty box below to add your bio and other information about your account like other social profiles and links.
![](https://www.minds.com/fs/v1/thumbnail/795699557608783872)
-Add things you want others to know.


Don't forget to click save, or everything you did is a waste of time, and on the waste of time note, don't use Internet Explorer.
![](https://www.minds.com/fs/v1/thumbnail/795699981107658752)
-Don't forget to save!!!


The next link will take you to trending channels on Minds, you can click this to subscribe to popular channels adding them to your newsfeed. We recommend joining groups that your interested and subbing channels you like inside thoes areas personally.
![](https://www.minds.com/fs/v1/thumbnail/795700629178458112)
-Find @MindsGaming

![](https://www.minds.com/fs/v1/thumbnail/795701611051941888)


If you're subscribing from inside a group or on the boostfeed you can just hover over the users icon and click subscribe .
![](https://www.minds.com/fs/v1/thumbnail/795702231561469952)
-Hover Sub.


To easily get back to your channel, click your icon in the top right corner
![](https://www.minds.com/fs/v1/thumbnail/795702398281359360)
-Click the icon you uploaded earlier.


You now only have a few links or suggestions on your channel you can "monetize" ypur posts by locking them for tokens, you can also click the X to remove the suggestion from your channel.
- [Lock Posts For Tokens]( https://www.minds.com/settings/wire)


Next you can become an affiliate a way to make tokens by bringing new users to Minds.
![](https://www.minds.com/fs/v1/thumbnail/795703560463777792)
-Affiliate Links


Next you may want to set-up your encrypted chat, this is the bottom right messenger tab, once you set this tab up it will no longer pop-up every-time you load a screen.
![](https://www.minds.com/fs/v1/thumbnail/795706125063069696)
-Set-up a new password.


You can now chat securely with other Minds, to do this just go to the users channel and click message .
![](https://www.minds.com/fs/v1/thumbnail/795708439148666880)


You can also search for users if you know their tag.
![](https://www.minds.com/fs/v1/thumbnail/795708523608866816)
-Search


That's it, you have set-up your channel now go out and explore the great world of Minds.

[Next! --> ](/#user-docs-Features)


