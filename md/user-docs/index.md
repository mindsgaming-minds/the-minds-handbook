# User Based Docs For Minds.com

To make a user based document outlining Minds.com relating to features, how-to, open source code and open source standards.

## Chapters: 
The chapters covered in this document are outlined below, you can add chapters or add on to chapters on the git for this project. To add to a chapter add a lettered value to the chapter below the last line of the current chapter, so if you wanted to add to “Chapter-1 Intro” you would label the new chapter “Chapter-1a {name}”. 


# [Chapter-1 Intro ](/#user-docs-Intro)
- About Minds.Com


# [Chapter-2 Creating an Account](/#user-docs-Creating-an-Account)
- How to create an account on Minds.com

# [Chapter-3 Features](/#user-docs-Features)
- Website features for Minds.com

- Chapter-3A Blogs

- Chapter-3B Videos

- Chapter-3C Images

- Chapter-3D Groups

- Chapter-3E Messenger

- Chapter-3F Video Chat

- Chapter-3G Conversations

- Chapter-3H Blocking

- Chapter-3I Boostin

- Chapter-3J Pinned Posts


# [Chapter-4 Installing](/#user-docs-Installing-Minds)
- Installing Minds.com


# [Chapter-5 Docs](/#user-docs-docs)
- User based docs on:

- Decentralized

- Community Owned

- Open Sourced

- Open Development

- Free Speech

- ORG

- Missing Docs & Info


# [Chapter-6 Tokens](/#user-docs-Tokens)
- Minds ERC-20 Token

- Chapter-6A Setting up you wallet. (MetaMask)

- Chapter-6B ERC-20 Tokens

- Chapter-6C Rewards

- Chapter-6D Paraphrase

- Chapter-6E Boost

- Chapter-6F Wire

- Chapter-6G Token Demand

- Chapter-6H Minds Token Documentation

---