## Chapter-3 Features

Minds.com has an abontance of features for use, below is the current list of features. They are always addeing and asking users to make a suggestions to features they should add, you can follow the current coming features on the Minds.com 
[RoadMap!](https://gitlab.com/groups/minds/-/roadmap).


## Chapter-3A Blogs

Users can create blogs on Minds, to do this select the "blog" icon from the top of the page and then select "create a new blog".

The features of blogging include;  


Formatting

First lets go over formatting. You can bring up your formatting options by simply highlighting the text you wish to format.
![](https://www.minds.com/fs/v1/thumbnail/795716085687820288)
-Formatting


Starting left to right your options are bold, underline, Strike-through.
# Heading2
## Heading3


If you hover over the icon you will see what each icon is for as well for justifying and breaking format, links and block-quote.


You will also see a + sign to project videos or images, or upload a image from your computer.
![](https://www.minds.com/fs/v1/thumbnail/795718378848034816)
-Plus

Here you can add a link for a video to play form popular sites and Minds media links or photo links. You can also upload a image by clicking the image icon and then the upload icon.
![](https://www.minds.com/fs/v1/thumbnail/795721872912982016)
-You can save your work for later by clicking save draft.

Before you click the publish you may want to set-up your blog license and other bogging post options including locking it to set a price to access by clicking the lightning bolt.


You can also set you META data clicking the down arrow below the posting options, this will set the blog title and display for when you share the link to others.
![](https://www.minds.com/fs/v1/thumbnail/795722490888650752)
-Meta Data

That's the basics, the rest is up to you!



## Chapter-3B Videos
- Users can upload videos via the paperclip icon on a post, currently, Minds only accepts videos under 15 minutes in length You can view your videos by going to the video gallery on your channel (below your bio, or the video icon at the top of desktop)  and click "View All."
Minds supports all major video files.



## Chapter-3C Images

- Users can upload Images via the paperclip. Minds accepts images below 15mb and accepts most major image files.



## Chapter-3D Groups
- Users can create groups for content or topics, click the navigation bar in the top-center of the screen and select "Groups." From there, you will be prompted to "Start a new group."
To invite a user to your group they must be subsribed to you, you must be the group owner or have gotten administrative privileges from the owner.  To invite a users click  "Members" above the bio, click on Invite, and you will see the search box to find those of your subscribers whom you would like to include. Click on a name to send an invitation.



## Chapter-3E Messenger
- Mind also has a messenge for users to directly communicate with one another in an encrypted chat.  To start using messenger clcik the messenger tab on the bottom right of the screen and "re-key" your password in settings. 


## Chapter-3F Video Chat
- Gatherings or (video conferencing is a feature inside group areas. Levieling the API from a fellow open-source project called Jitsi. Jitsi is a collection of free and open-source multi-platform voice, video conferencing and instant messaging applications for the web platform, Windows, Linux, Mac OS X and Android. All of these platforms, both web and mobile, will also be supported by Minds. 

- This is an essential communication and collaboration tool and will also serve as the foundation for future live streaming projects. You can use the feed to live stream to YouTube, but we are currently boycotting Google so we plan on eventually building our own so we don't need to rely on them. The Gathering panel appears just above the group feed when opened, and you can still navigate the entire group while using the camera.
![](https://www.minds.com/fs/v1/thumbnail/930571524174737408)

- The video conferencing capabilitiesl include: Screen Share, Dial-In, Raise Hand, Mute, Kick Out, Toggle View, Speaker Stats and more. Want to learn more, find out about about [Jitsi](https://jitsi.org/)


## Chapter-3G Conversations
- Tthe Conversations feature is also a feature inside the group area and can be used with Gatherings for real-time commenting during video conferences. Additionally, the Conversations panel is ever-present (with the ability to hide) on the right panel when you are inside a group. This facilitates much more dynamic communication and should result in much higher utilization of the tool. Because Conversations leverage the same codebase as our Comments, there will be a number of upgrades that will apply to both features. These include sub-comment threads, live updating (votes, replies), friendly timestamps and a new design. Also, comment and tag notifications will bring you to the exact spot in the thread for easier and more sensible navigation.
![](https://www.minds.com/fs/v1/thumbnail/930571386429456384)



## Chatper-3H Blocking
- Like most social medias Minds has a block feature, however this feature only stops interaction from users and does not make users you block on the network not seen. 



## Chapter-3I Boosting
- Boosting is one of the best things Minds offer, you can reach anyone on the network or at least you have the possibility to reach them.
Boosting to the network cost users Tokens, you can simply earn these Tokens or buy Tokens for ETH (1 Token= 1,000 Views) to boost to the network. (as a friendly note Tokens are not worth cash, but can be traded on exchanges.
To boost a post to the netowork create a post and selct the boost icon that will display in the bottom right corner of the post, fill in the amount of views and boost it to the network.



 ## Chapter-3J Pinned Posts
- Channels Owners,  group owners and group moderators will be able to pin up to three posts to the top of there channel or group feed ☺



[Next! ->](/#user-docs-iframe)