# User Token Documenation

The token frenzy has started and everyone is asking questions, stop take a breath, good? Alright lets begin in this blog we are going to try to explain everything that everyone is asking.

## Setting up you wallet. (MetaMask)


First download the extension for meta mask we have provided the link here for Firefox.

Click the fox that is now in your address bar.
-![](https://www.minds.com/fs/v1/thumbnail/825462603465650176)

Accept the terms and agreements after reading and you will be promoted to set-up a password, I recommend a separate password from Minds login or chat of course.
In your meta MaskMask menu click the 3 dots and copy address.

Navigate to your addresses in your wallet to begin, you should be on the following screen.
-![](https://www.minds.com/fs/v1/thumbnail/825461760979226624)

You can now provide your address for meta mask ignoring the use meta-mask option. (this will take you to the download provided above)
To see your points from the network make sure to change to the Rinkeby Test Net in the MetaMask drop-down.

OffChain or points
Offchain or non MetaMask is the same as in network points simply click "create a wallet"in the above menu and follow the steps provided :)


Adding Minds Tokens to MetaMask
To see your Minds onchain balance and use the Minds token you need to add it to your wallet. Open MetaMask go to the menu, click add token, select custom token and add the address:

0xb26631c6dda06ad89b93c71400d25692de89c068

If you are looking to set-up another wallet with Minds you may run into issues of it connecting and we have not tested any other wallets as test tokens were/are on the Rinkeby TesNet. We will be testing wallets on launch and will update this area as needed when done.

After you have set-up your wallet with Metamask or your preferred wallet if available you can now start buying, selling and exchanging tokens & ETH.


## ERC-20 Tokens

ERC-20 is a technical standard used for smart contracts on the Ethereum blockchain for implementing token, once they are listed on a marketplace you can sell or trade them for other crytpocurrencys or cash. The Minds Token is not listed on any market currently that I am aware.

A ECR-20 smart contract pretty much works like this you send X Ether and the system returns ERC20 tokens based on the code of worth assigned by the creator of the token.


What happens to your ether?

 Your sent ether gets transferred to the developers account and now belongs to them in exchange for there personalized token, in the simplest terms you are buying Minds tokens using your ether, you can use  the Minds Token to boost your content & wire to other accounts. Or in the hopes price on market places where you can trade it back at a higher rate than you purchased it for.


## Rewards
With Minds Wire you can assign rewards for users to send you tokens! This is going to be great for content creators!
Set-up you rewards in your bio.

-![](https://www.minds.com/fs/v1/thumbnail/826625240307163136)

Click the pencil to edit & check-mark to save
You can place rewards for an amount of  test tokens, maybe you just looking for some general support, or can make an image for a project, do an interview, and so on. Users can even support you on a monthly or one time base to try out your rewards or support you!
Lock posts by clicking the lightning bolt on your post before publishing and adding the token value, users who have wired this amount or more during the month will be able to see the post.

 # Paraphrase

When creating a wallet you will be prompted to save a paraphrase using both Meta Mask for Onchain or just creating a wallet with Minds.

Here we see the download screen after setting up your wallet with using the “create a new wallet feature” under addresses.

Minds paraphrase download screen.

Here we see a the Meta Mask paraphrase displayed when creating a new wallet using the meta mask service, it gives you the option to download or copy the address.

-![](https://www.minds.com/fs/v1/thumbnail/842815201286656000)

In both these cases it is very important to save this pass-phrase for future access to your account, if you lose this paraphrase at any point while having access you should transfer anything its holding to a new wallet where you do have this paraphrase

Saving and having it secured:

I save my paraphrase in a locked physical notebook.

*This paraphrase was used in a random test account with a random phone number

# Boost


What is Boost?

Boost is the in-house ad network on Minds, a way for you to gain views on your content from the network in exchange for Minds tokens or USD. It is secure, anti-surveillance. Read more: https://minds.com/boost
How do I Boost?

You can Boost your post, media or channel by clicking the "Boost" button on the desired content, and completing the prompt to request a specific number of views, what category (like "politics" or "animals") to target, and your payment method. Read more: https://minds.com/boost
What are the different types of Boost?

There are three different types of Boosts on Minds: 1) Newsfeed, 2) Sidebar and ) Channel, Newsfeed boosted posts are every twelfth post in the Newsfeed. Sidebar and Channel boosted posts meanwhile run along the left column of the Newsfeed. They are on three different counters so may occasionally appear simultaneously. Read more: https://minds.com/boost .


What is a Newsfeed Boost?

A Newsfeed Boost delivers guaranteed views on your posts in exchange for USD or Minds tokens. We place Newsfeed Boosts in the rotating queue at the top of the newsfeed or every twelve posts in the newsfeed. Read more: https://minds.com/boost


What is a Sidebar Boost?

A Sidebar Boost delivers guaranteed views on your channel, video, image or blog in exchange for USD or Minds tokens. We place Sidebar Boosts on the sidebar of the newsfeed and other user's content. Sidebar views are not counted on the newsfeed view counter. Read more: https://minds.com/boost


What is a Channel Boost?

A Channel Boost is a peer-to-peer (person to person) offer to pay another Minds channel in USD or Minds tokens in exchange for a guaranteed Remind (share). Channel Boost offers may be accepted or rejected. Use a Channel Boost to target specific communities with content. Build a great channel on Minds and earn revenue by accepting Channel Boost offers to Remind other users’ content to your audience. Read more: https://minds.com/boost


What is a Priority Boost?

A Priority Boost places your content at the very front of the line. This enables you to fulfill the number of views you request much more quickly and react to breaking or live events. Priority Boost rates are subject to surge pricing. The minimum price is twice regular Boost rates. Read more: https://minds.com/boost


How do I pay for a Boost?

You can pay for a Boost with USD or Minds tokens. If you choose to use Minds tokens, you can pay using your OnChain or your OffChain address.


What happens if I pay with my OnChain address?

If you choose to pay with your OnChain address, then your Boost transaction will be published to the public Ethereum blockchain. You will also need to pay a small Ethereum gas fee in order to process the transaction.


What happens if I pay with my OffChain address?

If you choose to pay with your OffChain address, then your Boost transaction will not be published to the public blockchain and will instead be stored on the Minds database. This database is immutable (cannot be changed) at the application level. You will not be required to pay any fees using this address, but spending limits will apply.


What counts as a view?

A view is defined by a post passing through the center of someone's screen.


How do I track the status of my Boost?

You can keep track of your boosted content in your Boost Console located in the top-bar navigation. In this console, you can track the status of your newsfeed Boosts, sidebar Boosts, and any offers you have sent or received for Channel Boosts.


How long does a Boost take to complete?

Boost completion time varies and is a function of our Boost backlog and site activity.


What is the Boost content policy?

Minds Boost Policy can be found in the Boost Policy section of our Terms of Service. Read more: https://www.minds.com/p/terms


## Wire


What is Wire?

Wire is the peer-to-peer payment and crowdfunding system on Minds. Minds tokens are accepted through Wire. Read more: https://minds.com/wire


Why would I Wire someone?

You can Wire another channel to support their content or to subscribe to their reward tiers.


How do I make my Wire recurring?

To make your Wire recur on a monthly basis, check the box that says "repeat this transaction monthly" before submitting.


How do I cancel a recurring Wire?

To cancel a recurring Wire subscription, go to the billing section of your settings and under the recurring payment section, click cancel.


How can I keep track of my Wire history?

You can track all of your Wire transaction history in your Wallet.


What is a reward tier?

Reward tiers are ways for content creators to build their own membership business on Minds. Creators can set different levels of reward tiers that provide different value for their supporters, similar to Patreon or other crowdfunding sites.


How do I create a reward tier?

You can set all of your reward tiers directly on your channel or Wallet by editing the widget called “Rewards”. In this widget, you can set the payment required and description for each level of rewards. Users must then Wire you the amount specified in order to access the rewards associated with each subscription.


How do I make content exclusive to my premium subscribers?

You have the option of making every post on Minds exclusive by clicking on the Wire icon on the post and specifying how much users must have Wired you in order to view the post. For example, you can set a post limited only to users who have Wired you 5 tokens that month. The post will appear locked to all users except those who meet the criteria.


## Token Demand


In this blog we are going to go over the stability of the Minds Token. If you have followed along with our blogs you already know what an ERC-20 token is and the basics of Minds Token.


Define Token Stability

Simply put its the ability of a token to hold its value, in this case the Minds token. Now we know that the Minds token ecosystem breaks down to views, so Minds token stability is based off Minds being able to serve its users views on their content where, one token is equal to one thousand views.


Variables

We must take in to account a few variables to understand if Minds can actually do this these being the following:

1.) Amount of views the network itself gets daily

*Minds had over 4 Million visits in August 2018

2.) What a view is.

*A view is depicted as an amount of time on a users screen. This should be a solid number without variable as this is what users are buying, VIEWS ; currently Minds has not set time for views and this is a major stability flaw, as seconds change for views so does the worth of a view!

3.) How many views are served a day

Average users stays on Minds about 6 minutes a day.

4.) How many are requested a day.

5.) Are all views served.

* You can track the content you boosted for views in your boost console to make sure they were completed.

Supply & Demand

The supply and demand matrix of Minds runs off if users want views outside there current groups and subscribers, currently you can not boost more than 5,000 views as this is what the system (supply of views) can handle per post. The demand of views of course is constant on a social atmosphere, we only need to worry about times and serving of the views.

Serving More Views

Minds could easily serve more views with simple tactics like placing a boost feed on the home page, or navigate news-feeds to boost-feeds by default, and I think this is something that they should consider looking into, not only would it help serve views but it would help users gain new supporters and subs.

Cost Per Mille (CPM) (the cost for 1,000 impressions, or views of your content)

The Formula to break down your CPM for Minds.com works like this;

	ETH = USD

	 1 Token = ETH

	1 Token = 1,000 views

	X ETH = 1,00 views


During our last guesstimate {10/9/2018} it costs about .15 cents to gain 1,000 views.

Do Rewards Hurt Demand?

I’d say overall no, and this is going to be the most debated part of this blog so let me explain why I say no.. As a bigger account I still boost my content, I.E blogs, videos, images that are original work by me I boost them, even though I have high numbers and get tokens the demand for views is still applicable.


Limits Do Hurt Your Rewards

On or around 10/18/2018 Minds added a limit to user interactions, this very well could hurt the reward pool and fuel to the network as the contribution poll is based off these actual interactions. 


Hypothesis

The Minds Token can be a stable utility token as long as Minds:

1.) Makes a user based spam system

2.) Identifies and defines a stable view count

3.) Identifies the amount of views served daily

4.) Serves views in a timely fashion.


Last Updated:  10/19/2018


====END USER DOCS ===


# Minds Token Documentation 

## Token 101
Download Whitepaper
How the new rewards system works

Minds rewards you with crypto for contributing to the network. The first step in joining our rewards program is to provide your mobile number. We convert your number into a hash ID using SHA-256 encryption and a salt key to create a unique identifier for your account. We do not store your number on our servers.

Rather than rewarding you with Points in real-time, we are now distributing our crypto rewards as a “Daily Reward Pool”. During the test network, we will be experimenting with how the Daily Reward Pool is calculated, but it will ideally be automated based on user participation once we move into the Main Ethereum network.

Every day, we measure your contributions to the network and you receive a “Contribution Score” which can be found in the Contributions section of your wallet. We then calculate how much you contributed to the network relative to the entire community, which determines the percentage of the Daily Reward Pool that you earn.

It is important to note that with the new system, you will only receive credit for unique interaction, and you can only earn credit from another unique user once per metric per day. (If a friend votes on my content 100 times in a day, I will only get credit for 1 vote). This is critical in preventing gaming and abuse of the system as we move into more valuable token rewards. We are essentially measuring how many unique users you reached in a given day.

Daily rewards will be distributed at 02:00 am GMT everyday into your OffChain address.
How we measure your contributions

We calculate your contribution score each day using the following weighted scoring system. Please note that the new system only gives credit for unique engagement (users who have provided a mobile number), so not all engagement you receive will be factored into your score.

Votes+1

Comments+2

Subscribers+4

Reminds+4

Referrals+10

OnChain Transactions+10 (receiver)

Check-ins+2

code +DevelopmentManually reviewed


OnChain vs. Offchain

OffChain tokens are stored on Minds servers, and they can be used the same way that you used your Points. There are no fees, and they can be used to Boost content for more views from the network or to Wire other creators who you support.

You now have the ability to withdraw these tokens into your OnChain address.

OnChain tokens are stored on the Ethereum public blockchain and are accessible from outside of the Minds platform, which is key for further decentralization. OnChain transactions, however, require a small Ethereum “gas” fee, which is the incentive for the Ethereum miners to process the transaction.

For OnChain configuration, you have the ability to use MetaMask (a browser plugin), your mobile device, an existing Ethereum address or you can create your own with Minds. We recommend using MetaMask for the most seamless user experience. You can change this address by clicking on the "Addresses" tab on the wallet menu

## Minds.com How-to docs for tokens

[How to buy MINDS tokens](https://www.minds.com/blog/view/871791809876131840)


[How to buy ETH on Coinbase](https://www.minds.com/blog/view/871789298595016704)


[How to buy ETH on Gemini](https://www.minds.com/blog/view/871787065656385536)

[How to setup your wallet with MetaMask](https://www.minds.com/blog/view/871783126122799104)

[How to setup your wallet without MetaMask](https://www.minds.com/blog/view/871784584725569536)

[Crypto launch, new apps and more!i](https://www.minds.com/blog/view/826188573910073344)


--- 

[<-Home PAGE!](/#user-docs-index)