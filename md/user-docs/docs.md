## User based documentation for "Minds.com"

Minds.com is an open soured, decentralized, community owned project?

Well this indeed the company’s long standing “philosophy”, PR slogan. Let investigate…

## Decentralized

Minds was launched somewhere in 2015 and was rumored to be supported by the hacking group “anonymous” for its decentralization,encryption, and anonymous nature.

Today in order to collect Tokens (Worth views to be seen) you must provide a phone number that is hashed and slated (encrypted)to register.

Minds to date is still not decentralized as no nodes or instances connect to the Minds network for posting, and the “maintained” nodes are full of bugs and issues from basics of site adminship to uploading.

## Community Owned

Minds launched a wefunder campaign for community ownership in 2017  with over 1,000 “investments” a million dollars invested into the program, yet Minds development team refuses to open development even to these investors that “own” Minds and stands behind “we have a small team” as they decline opening the development to users whom are interested., and today are a privately owned company. 


## Open Sourced

Minds is 100% FOSS, Free Open Source Software, and the code is publicly published on Gitlab for all to contribute to the program.

*Gitlab makes up most of Minds open source features.

Logging:

Minds is using Gitlab to log issues. This is a 3rd party system that is relayed to developers. To find out more about how this process works or ask more questions go here: https://about.gitlab.com/
- Log your first issue here:

![](https://www.minds.com/fs/v1/thumbnail/929535572691111936)

https://gitlab.com/groups/minds/-/issues

- Feature Request Taskboard:

![](https://www.minds.com/fs/v1/thumbnail/929535640794025984)

https://gitlab.com/groups/minds/-/boards/907151?&label_name[]=T%20-%20Feature

- Bug Taskboard:

![](https://www.minds.com/fs/v1/thumbnail/929535711351934976)

https://gitlab.com/groups/minds/-/boards/904771?&label_name[]=T%20-%20Bug

- Activity feeds.

![](https://www.minds.com/fs/v1/thumbnail/929535765008723968)

https://gitlab.com/groups/minds/-/activity

- RoadMap:

![](https://www.minds.com/fs/v1/thumbnail/929535801923735552)

https://gitlab.com/groups/minds/-/roadmap

You can also view things like milestones, analytics and more on Gitlab and of course have direct access to the git itself for the Minds code. These features are a great for the Minds development team as well as user involvement.


## Open Development

Minds Inc has a "Help Desk" where users can access ther current FAQ, Support group, as well as way to log issues to Gitlab directly from this area for community develpment.
https://www.minds.com/help

## Free Speech

On Minds your channel is your voice, and follows US laws for speech. Minds also has user Bill Of Ritgts (BOR) located at https://www.minds.com/p/billofrights. However the boost feature has many network based rules that conflicts with the user bill of rights 
and many users complain of network bias for allowing or approving of these boosts and does not follow the BOR or free speech model.


## ORG

You can find detailed documentation on https://oss.minds.com/ 


## Missing Docs & Info

Minds does not meet open source standards for a few key reasons.


# Users Have Requested.
- [ ] A human user based readable Changelog
"A changelog is a log or record of all notable changes made to a project. The project is often a website or software project, and the changelog usually includes records of changes such as bug fixes, new features, etc. Some open source projects include a changelog as one of the top level files in their distribution. "


# Bruce Perens, creator of The Open Source Definition, outlined six criteria an open standard must satisfy:

    - [X]Availability: Open standards are available for all to read and implement.
	- [ ]Maximize End-User Choice: Open Standards create a fair, competitive market for implementations of the standard. They do not lock the customer into a particular vendor or group.
	- [X] No Royalty: Open standards are free for all to implement, with no royalty or fee. Certification of compliance by the standards organization may involve a fee.
	- [X] No Discrimination: Open standards and the organizations that administer them do not favor one implementor over another for any reason other than the technical standards compliance of a vendor's implementation. Certification organizations must provide a path for low and zero-cost implementations to be validated, but may also provide enhanced certification services.
    - [ ]   Extension or Subset:Implementations of open standards may be extended, or offered in subset form. However, certification organizations may decline to certify subset implementations, and may place requirements upon extensions (see Predatory Practices).
	- [ ] Predatory Practices: Open standards may employ license terms that protect against subversion of the standard by embrace-and-extend tactics. The licenses attached to the standard may require the publication of reference information for extensions, and a license for all others to create, distribute, and sell software that is compatible with the extensions. An Open standard may not otherwise prohibit extensions.


# The Free Software Foundation Europe (FSFE) collaborated with other individuals and organizations in the tech industry, politics, and community to outline a different five-point definition. According to the FSFE, an open standard refers to a format or protocol that is:

	- [X ]subject to full public assessment and use without constraints in a manner equally available to all parties;
	- [ ]without any components or extensions that have dependencies on formats or protocols that do not meet the definition of an Open Standard themselves;
	- [ ]free from legal or technical clauses that limit its utilisation by any party or in any business model;
    - [ ] managed and further developed independently of any single vendor in a process open to the equal participation of competitors and third parties;
	- [ ] available in multiple complete implementations by competing vendors, or as a complete implementation equally available to all parties.
	
	
# The Open Source Initiative (OSI), the organization responsible for reviewing and approving licenses as Open Source Definition (OSD) conformant, says "an ‘open standard' must not prohibit conforming implementations in open source software." OSI provides a list of five criteria an open standard must satisfy. "If an ‘open standard' does not meet these criteria, it will be discriminating against open source developers," the site says:

	- [ ]No Intentional Secrets: The standard must not withhold any detail necessary for interoperable implementation. As flaws are inevitable, the standard must define a process for fixing flaws identified during implementation and interoperability testing and to incorporate said changes into a revised version or superseding version of the standard to be released under terms that do not violate the OSR.
	- [X]Availability: The standard must be freely and publicly available (e.g., from a stable web site) under royalty-free terms at reasonable and non-discriminatory cost.
	- [ ] Patents: All patents essential to implementation of the standard must:
	- [ ] be licensed under royalty-free terms for unrestricted use, or
	- [ ] be covered by a promise of non-assertion when practiced by open source software
	- [X] No Agreements: There must not be any requirement for execution of a license agreement, NDA, grant, click-through, or any other form of paperwork to deploy conforming implementations of the standard.
	- [ ] No OSR-Incompatible Dependencies: Implementation of the standard must not require any other technology that fails to meet the criteria of this Requirement.
	

[Next! ->](/#user-docs-Tokens)
	