## Getting your Iframe code for your website

Minds does not allow you to Ifrme your videos, blogs or posts into websites as it implements x-frame security for Iframe in its heaeder for its website.

[Read More About X-frame](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options)

 
 ## CDN 
 
 You can however use the Minds CDN <code>URL</code> to frame your videos into a website.
 
 You can do this by folling these steps:
 
 1.) Get your POST-ID from your newsfeed URL
 
 This is found after the newsfeed/ and is number string, so if my Newseed post is:
 
 <code>
 https://www.minds.com/newsfeed/841775459665170432
</code>


 My Post-ID would be:
 
 <code>
 841775459665170432
</code>


 Apply this to the end of the CDN url:
 
 <code>
 https://cdn-cinemr.minds.com/cinemr_com/POST-ID
</code>


 Then add your media file:
 
 <code>
 /.720mp4
</code>


 The frame URL in this example would be:
 
 <code>
 https://cdn-cinemr.minds.com/cinemr_com/841775459665170432/720.mp4 
</code>


## Concept Site

Skip this work and just grab the iframe:

  [Mframe](https://mframe.glitch.me/)
 
 ---

[Next! ->](/#user-docs-Installing-Minds)