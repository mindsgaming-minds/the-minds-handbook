# Blocking On Minds.Com

---

## How Blocking On Minds Works

Block is Linked to votes and comments on the post. So if you block a user they may not comment or vote on your posts and you will no longer see them in your news-feed.


Blocking is also linked to your subscribers as well, blocking a user will remove them from your subscribers.

You can block from a users post, or from a users channel.

Click the down arrow on a users post and select "Block".

![](https://www.minds.com/fs/v1/thumbnail/795726872384438272)


Unsubscribe is linked to the News-feed, when you subscribe the users post show on your news-feed.

If you UN-Subscribe you will no longer see them in your news-feed when they make new posts.


Hover over a users icon and to subscribe or unsubscribe.

![](https://cdn.glitch.com/edcaba76-3d3e-4908-a3d7-692c0166072b%2Funsub.png?v=1574217764922)

Mute on post mutes notifications on the post.

All posts can be viewed by the public unless marked unlisted or placed in a closed group, you can not block someone from viewing a public post.


**Boosted**

Blocking will NOT remove a user form your boost-feed.

---

**Reporting Accounts**

You can report accounts from the users channel page. On the users bio click the 3 dots then click report.

![](https://cdn.glitch.com/edcaba76-3d3e-4908-a3d7-692c0166072b%2Freport.png?v=1574218358634)

---