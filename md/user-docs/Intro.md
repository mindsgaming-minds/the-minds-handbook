## Chapter-1 Intro

Minds.com is an online social media located at [Minds.com](https://minds.com/) that uses an open source code available on [Gitlab](https://gitlab.com/minds) .

Wiki says;
"Minds is an open source and distributed social networking service, integrating the blockchain to reward the community with ERC-20 tokens for various contributions to the network. Users can use their tokens to promote their content or to crowdfund and tip other users by subscribing to them monthly in exchange for exclusive content and services. Minds describes itself as a "Crypto Social Network" that upholds "internet freedom"

Minds was co-founded in 2011 by Bill Ottman and Mark Harding as an alternative to social networks such as Facebook, who abuse digital rights.[6] It was also co-founded by John Ottman, Ian Crossland, and Jack Ottman. Minds launched to the public in June 2015.
In June 2017, the company raised over $1 million in the fastest equity-crowdfunded sale of all time on wefunder, https://wefunder.com/minds
On March 28, 2018, Minds exited Beta and the Minds Crypto Social Network was launched on mobile devices and on the web. In October 2018, Minds raised $6 millions in Series A funding from Medici Ventures, which is an Overstock.com subsidiary. Patrick M. Byrne, founder and CEO of Overstock.com, will join Minds’ board of directors.

[Next --> ](/#user-docs-Creating-an-Account)
