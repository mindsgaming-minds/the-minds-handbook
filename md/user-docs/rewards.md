# Setting Up Rewards

---

With Minds Wire you can assign rewards for users to send you tokens! This is going to be great for content creators!

## Set-up you rewards in your bio.


1.) Click the pencil to edit

![](https://cdn.glitch.com/edcaba76-3d3e-4908-a3d7-692c0166072b%2FRewards.png)

2.) Fill in the text area and amount for your rewards.


3.) Click the check-mark to save


You can place rewards for an amount of tokens, maybe you just looking for some general support, or can make an image for a project, do an interview, and so on. Users can even support you on a monthly or one time base to try out your rewards or support you!




**Lock posts by clicking the lightning bolt on your post before publishing and adding the token value, users who have wired this amount or more during the month will be able to see the post.**


> So go ahead and try it out WIRE someone some Tokens and support them today & set-up your rewards on your channel so users know what you can provided them with!

 [https://www.minds.com/wire]( https://www.minds.com/wire)