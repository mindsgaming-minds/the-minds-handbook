# What's a paraphrase?
----------------------

Don't lose it! It's only the most important thing to save!

When creating a wallet you will be prompted to save a paraphrase using both Meta Mask for Onchain or just creating a wallet with Minds.

Here we see the download screen after setting up your wallet with using the “create a new wallet feature” under addresses.

![](https://www.minds.com/fs/v1/thumbnail/842814673365319680)

Minds paraphrase download screen.


Here we see a the Meta Mask paraphrase displayed when creating a new wallet using the meta mask service, it gives you the option to download or copy the address.

![](https://www.minds.com/fs/v1/thumbnail/842815201286656000)


In both these cases it is very important to save this pass-phrase for future access to your account, if you lose this paraphrase at any point while having access you should transfer anything its holding to a new wallet where you do have this paraphrase

Saving and having it secured:

I save my paraphrase in a locked physical notebook.

This paraphrase was used in a random test account with a random phone number