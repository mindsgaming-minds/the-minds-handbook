---
# Building In Progress
---

(Proposed! - feedback welcomed!)

## The objectives

- Build a basic handbook for Minds.com 

- Provide a full stack of Minds Token Information

## Values

- Friendly

- Open to suggestions & feedback

## Principles

- Open Sourced 

- Community Built 

- User Based 

---

# Glitch Information
------------------------


## Working with Glitch

- Everyone who works on our handbook on glitch has access to everything: no second-class citizens.

## Glitch’s technology

- All updates and changes  are intant and hosted on the main site

## Running Glitch

- You will need a glitch account to contrubuite to this project you can also [install the git](/#README) if you want.
