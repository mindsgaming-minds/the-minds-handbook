# The Minds Handbook

---

Building a handbook for Minds.com

<details>
  <summary>View The Code (Click to expand)</summary>
  <iframe src="https://glitch.com/edit/#!/minds-handbook"  width="600" height="350" ></iframe>
  
  > [Go to the code](https://glitch.com/edit/#!/minds-handbook)

</details>

[View Our Values](/#values)

---

## Install

**Git**

You can clone our git and launch this handbook anywhere:

<code>
git clone https://api.glitch.com/git/minds-handbook
</code>
---

- Also [hosted on Gitlab](https://gitlab.com/mindsgaming-mg/the-minds-handbook)

---

**Remix**

Remix this project on [Glitch](https://glitch.com) and have it instantly up and running:

**[REMIX NOW](https://glitch.com/edit/#!/remix/minds-handbook)**

---

## Joining The Project

You can join this project and help keep it maintained

> Join Glitch

![](https://cdn.glitch.com/edcaba76-3d3e-4908-a3d7-692c0166072b%2FScreenshot_2019-11-18%20Glitch%20The%20friendly%20community%20where%20everyone%20builds%20the%20web.png?)

> [Request Access to Project](https://glitch.com/edit/#!/minds-handbook)

---

## Changelog

Only major changes, new folders and code updates are placed here. Small page updates and edtiting is instantally updated to the page.

- Create a handbook or wiki for tokens (! Thanks [Glitch](https://glitch.com/~eng-handbook) !)

- Edit & updated handbook with Minds token related blogs and information

- Updated Site with Minds User Docs

- Added Information From Minds FAQ page

- Changed name from <code>tokens-handbook</code> to <code>minds-handbook</code>

- Created [Gitlab Copy of Site](https://gitlab.com/mindsgaming-mg/the-minds-handbook)

- Added Monetization Information For Pro User

---
