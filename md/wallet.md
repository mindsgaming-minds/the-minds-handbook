## Setting up you wallet. (MetaMask)

- Download the extension for [MetaMask](https://metamask.io/) we have provided the link here for Firefox.

Click the fox that is now in your address bar; 
![](https://www.minds.com/fs/v1/thumbnail/825462603465650176)

Accept the terms and agreements after reading and you will be promoted to set-up a password, I recommend a separate password from Minds login or chat of course.

In your meta MaskMask menu copy your ETH address.


[Navigate to "addresses"](https://www.minds.com/wallet/tokens/addresses) in your wallet to begin, you should be on the following screen.

![](https://www.minds.com/fs/v1/thumbnail/825461760979226624)



You can now provide your address for meta mask ignoring the (use MetaMask option, this will take you to the download provided above)


To see your points from the network make sure to add Minds Tokens to MetaMask,


To see your Minds onchain balance and use the Minds token you need to add it to your wallet. Open MetaMask go to the menu, click add token, select custom token and add the address:

<code>
0xb26631c6dda06ad89b93c71400d25692de89c068
</code>