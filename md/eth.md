# ETH & Tokens - The breakdown.

---

The token frenzy has started and everyone is asking questions, stop take a breath, good? Alright lets begin in this blog we are going to try to explain everything that everyone is asking.

## Setting up your wallet.

Minds recommends using MeataMask and we already have blog for [setting-up your wallet here](https://minds-handbook.glitch.me/#wallet).

If you are looking to set-up another wallet with Minds you may run into issues of it connecting and we have not tested any other wallets as test tokens were/are on the Rinkeby TesNet. We will be testing wallets on launch and will update this area as needed when done.

After you have set-up your wallet with Metamask or your preferred wallet if available you can now start buying, selling and exchanging tokens & ETH.

## ERC-20 Tokens

ERC-20 is a technical standard used for smart contracts on the Ethereum blockchain for implementing token, once they are listed on a marketplace you can sell or trade them for other crytpocurrencys or cash. The Minds Token is not listed on any market currently that I am aware.

A ECR-20 smart contract pretty much works like this you send X Ether and the system returns ERC20 tokens based on the code of worth assigned by the creator of the token.

What happens to your ether?

Your sent ether gets transferred to the developers account and now belongs to them in exchange for there personalized token, in the simplest terms you are buying Minds tokens using your ether, you can use the Minds Token to boost your content & wire to other accounts. Or in the hopes price on market places where you can trade it back at a higher rate than you purchased it for.

## Adding Minds Tokens to MetaMask

To see your Minds onchain balance and use the Minds token you need to add it to your wallet. Open MetaMask go to the menu, click add token, select custom token and add the address:

<code>
0xb26631c6dda06ad89b93c71400d25692de89c068
</code>

## Buying Minds Tokens

Minds has a blog on [buying tokens here](https://www.minds.com/blog/view/871791809876131840) with MetaMask & we haven't tested the method with any wallet as the token buying page is not open yet buy generally you should follow the Minds blog as follow

Step 1 - [Visit the MINDS token page.](https://minds.com/token)

Step 2 - Make sure you have your wallet open and can access it.

Step 3 - Input the amount of MINDS tokens you wish to purchase, or the amount of ETH you wish to spend. Then click "Purchase.

Step 4 - Check the boxes in the prompt and then press "Purchase" again.

Step 5- Non-MetaMask users will now be prompted with a popup to execute the transaction. Enter your private key and press "Complete". Then, enter the amount of gas you wish to send, then press
"Approve". For information on how much gas to send, you can visit [https://ethgasstation.info](https://ethgasstation.info).

## Beta Conversion Rate

Minds is Giving 100% value to test tokens to registered users, this means that you can covert your test tokens to Minds Tokens!

## How To Make Money

You can NOT sell Minds tokens on the Minds network. To sell a token it must be listed on an exchange, so you must have tokens in your on-chain address Here we are going to look at two exchanges.

**Buying & Sell Tokens**

- As covered eariler buying and selling cost a gas fee (ETH) for any outgoing transaction

[- UniSwap](https://uniswap.exchange/swap)

Step 1.) Open Uniswap

Step 2.) Make sure your MetaMask is unlocked.

Step 3.) Select "Input" and manually enter the Minds Token address

3.) Select output [ETH]

4.) Click Swap to liqufiy

[- ForkDelta](https://forkdelta.app)

ForkDelta is a decentralized exchange where you can trade ECR-20 Tokens for ETH to do this we must add the Minds Token to the exchange.

Step 1.) Make an Account on ForkDelta

Step 2.) [Open ForkDelta](https://forkdelta.app/#!/trade/0xb26631c6dda06ad89b93c71400d25692de89c068-ETH) (With the Minds address!)

Step 3.) Make sure your MetaMask is unlocked, go to deposit tokens enter the amount and make your deposit.

Now we can deposit tokens to sell or find tokens to buy from other users for ETH.
