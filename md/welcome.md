# Welcome to the Minds Handbook!

![](https://cdn.glitch.com/edcaba76-3d3e-4908-a3d7-692c0166072b%2Flogo.svg)

We've [open-sourced](/#README) this handbook so that anyone can learn and educate others about Minds. It's part of our effort to keep users informed and impowered.

You can [Remix this Handbook](https://glitch.com/edit/#!/remix/minds-handbook) to get your own copy that your team can modify and customize. This handbook is licensed under a [Creative Commons Attribution](https://creativecommons.org/licenses/by/4.0/) license. [![CC By](https://cdn.glitch.com/f55866fa-2ded-48bc-aa22-2f7c25415551%2Fby-line.svg?1539695333634)](https://glitch.com/edit/#!/minds-handbook?path=LICENSE.md:3:0)

---

All information in this handbook is provided by users of [Minds](https://minds.com/) and not reviewed by "Minds INC", developers or admins.

## Getting Started With Tokens

- [Setting Up Your Wallet](/#wallet)
- [Basics of ETH & Minds Tokens](/#eth)
- [Converting Minds Tokens To ETH, BTC, And More. ](/#convert)
- [How the reward system works](https://www.minds.com/wallet/tokens/101)

## Getting Started With Minds

- [Intro](/#user-docs-Intro)
- [Mobile App](/#mobile)
- [Installing Minds](/#user-docs-Installing-Minds)


## Monetization

- [Minds is now Monitized](/#pro-pay-to-play)
- [Minds Pro](https://minds.com/pro)


## Team Hubs

- [Minds](https://www.minds.com/groups/profile/886268855514841088/feed)
- [Riot](https://riot.im/app/#/room/#mcosi.open-source.zone:matrix.org)
- [Glitch](https://glitch.com/@minds)

---

## Objectives & Values

- [Values](/#values)


## Editing this handbook

- This Handbook is made with Glitch: That means anyone can modify and customize their own copy or help maintain this copy.
- Here's a [markdown cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet), in case you need it.

## Contrubutors

- Become a contrubutor by [Joining The Project](https://glitch.com/edit/#!/minds-handbook)
