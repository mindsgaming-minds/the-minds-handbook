# Converting Minds Tokens To ETH, BTC, And More.
-------------------------------------------------

We are going to start with Withdrawling Minds tokens from your Minds account to Metamask.


# Withdrawl

To move your Minds offchain tokens to the blockchain,

Make sure that you have unlocked your Metamask and added the Minds Token to your MetaMask wallet.

The Minds Token address is:
<code>

    0xb26631c6dda06ad89b93c71400d25692de89c068
</code>

Next we need to go to the Minds withdrawl console, to do this click the [Bank Icon,then withdrawl on Minds.](https://www.minds.com/wallet/tokens/withdraw)

Follow the Metamask promts to pay the gas fee.

Once completed the Minds token will show in your Metamask wallet.

Now your Minds Tokens are in your Metamask wallet!


# Exchanging

We will be using [Uniswap](https://uniswap.exchange/swap) to exchange tokens.

Under "Swap" Click the dropdown for "input" and manually past the Minds token address.

Address Reminder:

    0xb26631c6dda06ad89b93c71400d25692de89c068

Next Select "ETH" for your "Output" back to your Metamask wallet.

Make sure you are connected (top right corner) and click SWAP!

You have now exchanged your tokens for ETH and can cash out (exchange) your ETH for USD with cointraders like Coinbase.


[Walk Through Video](https://www.minds.com/api/v1/media/1038206560931024896/play?s=modal&res=720)

