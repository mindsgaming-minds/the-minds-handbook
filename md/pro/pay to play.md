# Minds is now Monitized (Pay To Play)
---------------------------------------

Minds is now monitized with a play to pay system.

In one of our last blogs about Minds, Minds Going From FOSS to OSS (“Why yes you’re on-chain") we talked a little about play to pay systems and how they are not a new concept and the second class citizenship that they imply.

In this blog we will be capitalizing on this with Minds new pay to play system or as they call it "Minds Pro", Minds Pro launched with a surged cost this week and only secures the new pay to play system that Minds is now a part of.

## Pay To Play

![](https://www.minds.com/fs/v1/thumbnail/1048019773193158656)
Pay-to-play refers to situations where one must exchange money to engage in an activity or to earn favors or influence.

Yep, sounds right on the money here (pun implied). On Minds you can become a pro channel, and pro channels earn the biggest favor and activity that a system could offer, montization. Montization offers a channel the chance to make money on the system or network that they engage with.

Most networks however dont charge to become montized, although they may take a percentage of earnings in order to compensate for the service and a reason exsits for this layout.

## Pay To Play Always Fails
![](https://www.minds.com/fs/v1/thumbnail/1048020544504700928)

Many reasons exist why play to pay systems do not work but we are only going to go over the top reasons in our view here.

### 1.) It is an investment

Many that intertwine with making money online with networking may have a stable income however taking a risk for the possibilty of a gain is simply not appealing.

### 2.) High Cancellation

Those that do take the risk will expect results in the first few months, of not a gain enough to break even may keep them enrolled but overall high cancellation fees happen in pay to play systems.

### 3.) Benefits

Many users that enrol in play to pay systems will realize that they are a key feature of the system and benefit the system or network more than the system or network is benefiting them. In short they are paying to provide views to the system or network and not making a significant or any gain for doing so.

### 4.) Not competitive

Most play to pay systems are not competitive to free to play counterparts that run off percentages of gain over a subscription model, however Minds is pretty innovative in this department.

### 5.) Bugs

When a user pays for something they expect top customer service and features with little and preferably no issues with the service. This brings the issue of prioritizing issues, what bug is going to be fixed first a pro bug or general user bug, expressly with a service. As general issues are put on the backburner to fix issues or services with a pro service general users get frustrated bringing less engagement and views to the pro users.

## Evaluate Your Channel
![](https://www.minds.com/fs/v1/thumbnail/1048020053581533184)

If you are thinking to join the pro service as a general user you may want to first evaluate your channel, remember that this is a risk and not a guaranteed gain or percentage you may be familiar with.

Take the following into account before opening your pro account.

### 1.) Why do I want to go pro?

If your a business or not looking to make a gain and only look more professional or flashy for the sake of it pro is definitely for you. If your looking for benefits keep reading.

### 2.) How many views do I get a month?

You can now see (in canary) the amount of views you have per month, day, and year with the analytics console. For evey 1,000 views you get a dollar subtract that from your total month cost, are you breaking even?

### 3.) Do I mass promote

If your a major content pusher you may also make money off referrals, so you can also do this breakdown for how many a month you get to join (and verify the account) to help you break even for \$0.10 an account.

### 4.) Are you a slaes man?

Are you looking to sell Minds plus, well if so you can also make money from this, you know the old you by this I make money you make money set-up not saying pyramid just saying.

### 5.) Do you have a website already?

If you already have a website you may want to itemize the cost to have Minds installed on your own server with a plus package over using the Minds pro website builder just based off rates itself, of course as Minds is still not decentralized (if it will ever become) this cant be projected onto Minds network like you may be able to do on other decentralized systems.
Far from the vision of Minds.

Minds goal when it launched was to impower all the users of the network in a free open space. It has slowly moved from the original goals and vision to what we have today, a simple pay to play social media, a game for views if you will not an free open space that was once envisioned by its creators.

#DontBeEvil

## Closing

My personal views aside I think that someone could vastly improve on the current layout and market of Minds and its pro package with a percentage of gains free to play scheme over a subscription based model like many other networks.


-  Dec 1, 2019

<b>[Join Pro](https://minds.com/pro)</b>