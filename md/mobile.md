# Mobile Access
----------
## Intro

Two way to access Minds via your mobile device are available.

1.) Web App
A web 

## Mobile Web App

The mobile web version of Minds accessed via a mobile browser (Like Firefox Mobile Web) provides most features and aspects of Minds, and similar functionally of the desktop version.

**Features**

A.) Tokens -[Y]

B.) Playable Links (YouTube, SoundCloud) -[Y]

C.) Mature/Adult Content -[Y]

D.) Blogs -[N]


## Installing Mobile App

A.) Direct download from Minds at [https://minds.com/mobile](https://www.minds.com/mobile) to receive the full version with adult content and token/boost use.

B.) Your Mobile "App Store" also should have a download for the Minds App. Both Andriod & Apple provide a limited version of Minds that does not allow adult content. 

**The Apple app (from the "store" also doesn't allow use of Minds Tokens. These restrictions are due to the terms of service of the service providing the download**


## Passwords

You can set both your messenger password as well as your account password in the mobile app settings.

Resetting passwords may require you to clear your application cache if logged in via other applications.

## Mobile App Issues

You can report Mobile app issues via the "Help & Support Group", the "Help Desk" or on GitLab.

Help & Support Group:

[https://www.minds.com/groups/profile/100000000000000681/feed](https://www.minds.com/groups/profile/100000000000000681/feed)

Help Desk:

[https://www.minds.com/issues/report](https://www.minds.com/issues/report)

Gitlab: 

[https://www.gitlab.com/minds/mobile-native/issues](https://www.gitlab.com/minds/mobile-native/issues)


## Mobile App Known Issues

- Blank Feeds

- No Playable Links (YouTube, Soundcloud)

**Notice**

Please inform us if issues or things you would like covered in this area as we use firefox mobile web to access Minds when on the go.
Minds Mobile App
