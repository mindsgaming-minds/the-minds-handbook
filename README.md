# Welcome to the Minds Handbook

## [ `Show Site` ](https://minds-handbook.glitch.me)

---

![The Minds Handbook](https://cdn.glitch.com/edcaba76-3d3e-4908-a3d7-692c0166072b%2Flogo.svg?v=1574101026716)

Building a user based handbook for Minds.com

---

# Install

**Git**

You can clone our git and launch this handbook anywhere:

<code>
https://gitlab.com/mindsgaming-mg/the-minds-handbook.git</code>

**Remix**

Remix this handbook on glitch and have it instantly up and running:

[Remix](https://glitch.com/edit/#!/remix/minds-handbook)

---

# Editing & Adding Information

<-- Add & edit docs using markdown under the <code>md</code> folder

**Add File**

<-- Click "New File"

Type <code>md/yourfilename.md</code> and click enter

Fill in the markdown for your file.

**Add Folder**

<-- Click "New File"

Type <code>md/yourfoldername/yourfilename.md</code>

Fill in the markdown for your file.

---
